package com.example.mikuc.coffelobby.ClassCategoryFood;

import com.example.mikuc.coffelobby.R;

public class Cakes {

    public static final String EXTRA_CAKENO="EXTRA_CAKENO";
    private String name;
    private String content;
    private int id;

    private Cakes(String n, String c, int i)
    {
        this.name=n;
        this.content=c;
        this.id=i;
    }

    public String toString()
    {
        return this.name;
    }
    public String getContnet()
    {
        return content;
    }
    public int getImage()
    {
        return id;
    }
    public String getName()
    {
        return name;
    }

    public static final Cakes[] cakes={
            new Cakes("Brownie"," ciasto czekoladowe typowe dla kuchni amerykańskiej. " +
                    "Nazwa pochodzi od charakterystycznego, ciemnobrązowego koloru brownie. " +
                    "Ciasto przygotowuje się z gorzkiej czekolady, jajek, cukru, masła " +
                    "i niewielkiej ilości mąki. ", R.drawable.brownie),
            new Cakes("CheseCake"," rodzaj ciasta deserowego lub deseru uformowanego " +
                    "na kształt ciasta, którego głównym składnikiem jest biały ser",R.drawable.chesscake),
            new Cakes("Skiitels","Ciasto z dodatkiem cukierków skitelse",R.drawable.skitels),
    };



}