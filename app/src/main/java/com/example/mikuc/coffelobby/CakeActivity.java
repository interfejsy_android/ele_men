package com.example.mikuc.coffelobby;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikuc.coffelobby.ClassCategoryFood.Cakes;


public class CakeActivity extends Activity {
    public static final String EXTRA_CAKENO="EXTRA_CAKENO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        int cakeno = (Integer) getIntent().getExtras().get(EXTRA_CAKENO);
        Cakes cake = Cakes.cakes[cakeno];

        ImageView iv = (ImageView) findViewById(R.id.image_drink);
        TextView tx1 = (TextView) findViewById(R.id.tx1);
        TextView tx2 = (TextView) findViewById(R.id.tx2);
        iv.setImageResource(cake.getImage());
        iv.setContentDescription(cake.getName());
        tx1.setText(cake.getName());
        tx2.setText(cake.getContnet());
    }
}
