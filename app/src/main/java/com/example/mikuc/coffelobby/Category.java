package com.example.mikuc.coffelobby;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.mikuc.coffelobby.ClassCategoryFood.Drink;


public class Category extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView lv=getListView();
        ArrayAdapter<Drink> listAdapter=new ArrayAdapter<Drink>(this,android.R.layout.simple_list_item_1, Drink.drinks);
        lv.setAdapter(listAdapter);

    }

    @Override
    public void onListItemClick(ListView listView, View itemView, int position, long id){
        Intent intent=new Intent(Category.this, DrinkActivity.class);
        intent.putExtra(Drink.EXTRA_DRINKNO, (int) id);
        startActivity(intent);

    }

}
