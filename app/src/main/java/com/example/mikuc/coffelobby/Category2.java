package com.example.mikuc.coffelobby;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.mikuc.coffelobby.ClassCategoryFood.Cakes;


public class Category2 extends ListActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView lv = getListView();
        ArrayAdapter<Cakes> arrayAdapter = new ArrayAdapter<Cakes>
                (this, android.R.layout.simple_list_item_1, Cakes.cakes);
        lv.setAdapter(arrayAdapter);

    }

    @Override
    protected void onListItemClick(ListView listView, View itemView, int position, long id) {
        Intent intent = new Intent(Category2.this, CakeActivity.class);
        intent.putExtra(Cakes.EXTRA_CAKENO, (int) id);
        startActivity(intent);
    }
}
