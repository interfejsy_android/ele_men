package com.example.mikuc.coffelobby;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikuc.coffelobby.ClassCategoryFood.Drink;


public class DrinkActivity extends Activity {
    public static final String EXTRA_DRINKNO="EXTRA_DRINKNO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);


        int drinkno = (Integer) getIntent().getExtras().get(EXTRA_DRINKNO);
        Drink drink = Drink.drinks[drinkno];
        ImageView iv = (ImageView) findViewById(R.id.image_drink);
        TextView tx1 = (TextView) findViewById(R.id.tx1);
        TextView tx2 = (TextView) findViewById(R.id.tx2);
        iv.setImageResource(drink.getImage());
        iv.setContentDescription(drink.getName());
        tx1.setText(drink.getName());
        tx2.setText(drink.getContnet());




    }

}
