package com.example.mikuc.coffelobby;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AdapterView.OnItemClickListener itemClickListener=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int i, long l) {

                switch((int)l)
                {
                    case 0:
                        Intent intent=new Intent(MainActivity.this, Category.class);
                        startActivity(intent); break;

                    case 1:
                        Intent intent2=new Intent(MainActivity.this, Category2.class);
                        startActivity(intent2); break;

                }
            }
        };

        ListView lv=findViewById(R.id.listView);
        lv.setOnItemClickListener(itemClickListener);
    }
}
