package com.example.mikuc.coffelobby.ClassCategoryFood;

import com.example.mikuc.coffelobby.R;

public class Drink {

    public static final String EXTRA_DRINKNO="EXTRA_DRINKNO";
    private String name;
    private String content;
    private int id;

    private Drink(String n, String c, int i)
    {
        this.name=n;
        this.content=c;
        this.id=i;
    }

    public String toString()
    {
        return this.name;
    }
    public String getContnet()
    {
        return content;
    }
    public int getImage()
    {
        return id;
    }
    public String getName()
    {
        return name;
    }

    public static final Drink[] drinks={
      new Drink("Late","Czarne espresso z gorącym mlekiem i mleczną pianką", R.drawable.latte),
      new Drink("Capucino","Czarne espresso z gorącym  spieninoym mlekiem",R.drawable.cappuccino),
      new Drink("Espresso","Czarna kawa ze świeżo mielonych ziaren najwyższej jakości",R.drawable.filter),
    };



}
